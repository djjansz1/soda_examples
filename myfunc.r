#function to pause for n secnods and print the time to the console window
pause <- function(sec) for (i in 1:sec)
{ cat(format(Sys.time(), "%X"),"\n")
  date_time<-Sys.time() #prints the time each second for x seconds
  while((as.numeric(Sys.time()) - as.numeric(date_time))<1){} 
}

#define some objects related to the Canadian provinces
provinces<-c("AB","BC","MB","NB","NL","NS","NU","NT","ON","PE","QC","SK","YT")
